import os
import sys
from pyquery import PyQuery as pq
from kyssModule import KyssModule

class RyzomNews(KyssModule):
	
	def getUsedZones(self):
		return {"main": 100}
	
	def setup(self):
		self.log("Setup News")
		try:
			html = pq("https://app.ryzom.com/app_releasenotes/index.php?lang="+KyssModule.lang+"&ryztart_version="+KyssModule.VERSION, headers={"user-agent": "Ryztart"})
		except:
			html = ""
		else:
			body = html(".ryzom-ui-body")
			html = body.html()

		self.setZone("main", html)
		KyssModule.setup(self)